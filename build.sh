#!/bin/bash

BUILD_DIRECTORY="build"

rm -rf $BUILD_DIRECTORY
mkdir $BUILD_DIRECTORY
npx asciidoctor-revealjs slides.adoc
sed -i -e 's/node_modules\///g' slides.html
rm slides.html-e
mv slides.html $BUILD_DIRECTORY/index.html
cp -r img $BUILD_DIRECTORY
mkdir $BUILD_DIRECTORY/reveal.js
cp -r node_modules/reveal.js/dist $BUILD_DIRECTORY/reveal.js/dist
cp -r node_modules/reveal.js/plugin $BUILD_DIRECTORY/reveal.js/plugin
